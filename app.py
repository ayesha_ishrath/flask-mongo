from flask import Flask
from flask import request
from pymongo import MongoClient
from flask_restful import Resource, Api
from bson.objectid import ObjectId

client = MongoClient('localhost',27017)
db = client.todo
tasks = db.task
app = Flask(__name__)
api = Api(app)

class User(Resource):
   def get(self):
        #id = request.values.get("_id")
        #id = ObjectId(id)
        user =  request.values.get("username")
        psw = request.values.get("password")
        result = tasks.find({"username":user,"password":psw}).count()
        if (result==0):
            return "Who are you? -_-",201
        else:
            return "Hey You! :)",200
        #user =  request.values.get("name")
        #psw = request.values.get("score")
        #if (user=="Virat") and (psw=="200"):
        #    return "Hey You! :)",200
        #else:
        #    return "Who are you? -_-",201
api.add_resource(User,"/auth")

class Task(Resource):
    def get(self):
        data=[]
        for task in tasks.find():
            task["_id"] = str(task["_id"])
            data.append(task)
        return data
    def post(self):
        name_id = request.values.get("username")
        pword = request.values.get("password")
        tasks.insert({"username":name_id,"password":pword})
        return "inserted", 201
    def delete(self):
        id = request.values.get("id")
        id = ObjectId(id)
        tasks.remove({"_id":id})
        #name = request.values.get("name")
        #tasks.remove({name:name})
        return "deleted",200
    def put(self):
        score = request.values.get("score")
        name = request.values.get("name")
        tasks.update_one({"name":name},{ "$set":{"score":score}})
        return "updated" ,200
api.add_resource(Task,"/")  #Use the class name

if __name__ == "__main__":
    app.run(debug=True)
